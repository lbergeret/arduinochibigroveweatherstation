/*
Note about Freaklab Chibi board:
  Two pins on the analog input connector are not available for use except for standard digital I/O.
  These are pins Analog 2 and Analog 3. Analog 2 controls the sleep mode and Analog 3 controls the chip
  select for communications between the microcontroller and radio IC. If the wireless functionality is not used,
  these pins are available as standard digital I/O but not as analog inputs. If possible, its best to avoid using
  these pins. If the wireless functionality is being used, the SPI bus is also required to communicate with the radio.
  That means that digital pins 10-12 (PB3 to PB5 or MOSI, MISO, and SCLK) will be dedicated SPI pins.
 */

#include <Wire.h>
#include <Adafruit_BMP085.h>
#include <DHT22.h>
#include <chibi.h>
#include <limits.h>

#include <MemoryFree.h>

// Watchdog
#include <avr/io.h>
#include <avr/wdt.h>

#define MONITOR_ADDRESS 0x2001
#define RECEIVER_ADDRESS BROADCAST_ADDR //0x2000
#define RECEIVER_CHANNEL 0

//#define USE_BUSZZER
#define USE_BMP085
#define USE_DHT11
#define USE_MQ5
#define USE_MQ7

// Buzzer ------------------------------------------------------------
#define BUZZER_DPIN 3
int buzzer_alert = 0;

// Dust --------------------------------------------------------------
#define DUST_DPIN 8
unsigned long dust_duration;
unsigned long dust_starttime;
unsigned long dust_sampletime_ms = 30000;

unsigned long dust_lowpulseoccupancy = 0;
float dust_ratio = 0;
float dust_concentration = 0;

// Pressure ----------------------------------------------------------
#ifdef USE_BMP085
Adafruit_BMP085 bmp;
unsigned long bmp85_starttime;
unsigned long bmp85_sampletime_ms = 5000;
#endif

float pressure = 0;
int temperature1 = 0;

// Humidity ----------------------------------------------------------
#ifdef USE_DHT11
#define DHT22_DPIN 7
unsigned long dht11_starttime;
unsigned long dht11_sampletime_ms = 5000;
DHT22 dht(DHT22_DPIN);
#endif

unsigned int humidity = 0;
int temperature2 = 0;

// Gaz --------------------------------------------------------------
#ifdef USE_MQ5
#define MQ5_INPUT A0
unsigned long mq5_starttime;
unsigned long mq5_sampletime_ms = 5000;
#endif

unsigned int gaz = 0;

// CO Gaz --------------------------------------------------------------
#ifdef USE_MQ7
#define MQ7_INPUT A1
#define MQ7_TOG_DPIN 9

unsigned long mq7_starttime;
unsigned long mq7_heattime_ms = 60000;
unsigned long mq7_sampletime_ms = 90000;
unsigned long mq7_timer_ms = mq7_heattime_ms;
bool mq7_state = HIGH;
#endif

unsigned int cogaz = 0;

// Chibi ------------------------------------------------------------
unsigned long chibi_starttime;
unsigned long chibi_sendtime_ms = 30000;

// Global definitions -----------------------------------------------
static char string_buffer[128];

// Calculate elapsed time. this takes into account rollover.
unsigned long elapsedTime(unsigned long startTime) {
  unsigned long stopTime = millis();

  if (startTime >= stopTime) {
    return startTime - stopTime;
  } else {
    return (ULONG_MAX - (startTime - stopTime));
  }
}

// Setup ------------------------------------------------------------
void setup() {
#ifdef USE_BUSZZER
  pinMode(BUZZER_DPIN, OUTPUT);
  digitalWrite(BUZZER_DPIN, LOW);
#endif

  chibiInit();
  //chibiSetChannel(RECEIVER_CHANNEL);
  chibiSetShortAddr(MONITOR_ADDRESS);

  Serial.begin(9600);

  pinMode(DUST_DPIN,INPUT);
  dust_starttime = millis();

#ifdef USE_BMP085
  bmp85_starttime = millis();
  bmp.begin();
#endif

#ifdef USE_DHT11
  dht11_starttime = millis();
#endif

#ifdef USE_MQ5
  mq5_starttime = millis();
#endif

#ifdef USE_MQ7
  mq7_starttime = millis();
  pinMode(MQ7_TOG_DPIN, OUTPUT);
  digitalWrite(MQ7_TOG_DPIN, mq7_state);
#endif

  chibi_starttime = millis();

  delay(300);//Let system settle
  Serial.println("Chibi Groove Indoor Monitor\n\n");
  delay(700);//Wait rest of 1000ms recommended delay before
}

// Loop -----------------------------------------------------------
void loop() {
  dust_duration = pulseIn(DUST_DPIN, LOW);
  dust_lowpulseoccupancy = dust_lowpulseoccupancy+dust_duration;

  if (elapsedTime(dust_starttime) > dust_sampletime_ms) {
    dust_ratio = dust_lowpulseoccupancy/(dust_sampletime_ms*10.0);  // Integer percentage 0=>100
    dust_concentration = 1.1*pow(dust_ratio,3)-3.8*pow(dust_ratio,2)+520*dust_ratio+0.62; // using spec sheet curve
    Serial.print("Dust = ");
    Serial.print(dust_lowpulseoccupancy);
    Serial.print(",");
    Serial.print(dust_ratio);
    Serial.print(",");
    Serial.println(dust_concentration);
    dust_lowpulseoccupancy = 0;
    dust_starttime = millis();
  }

#ifdef USE_BMP085
  if (elapsedTime(bmp85_starttime) > bmp85_sampletime_ms) {
    pressure =  bmp.readPressure();
    temperature2 = bmp.readTemperature()*10;
    Serial.print("BMP: Temperature = ");
    Serial.print(temperature2);
    Serial.print(" C ");
    Serial.print("Pressure = ");
    Serial.print(pressure/100);
    Serial.println(" hPa");
    bmp85_starttime = millis();
  }
#endif

#ifdef USE_DHT11
  if (elapsedTime(dht11_starttime) > dht11_sampletime_ms) {
    DHT22_ERROR_t errorCode;
    errorCode = dht.readData();
    switch(errorCode)
    {
    case DHT_ERROR_NONE:
      humidity = dht.getHumidityInt();
      temperature1 = dht.getTemperatureCInt();
      // Alternately, with integer formatting which is clumsier but more compact to store and
      // can be compared reliably for equality:
      sprintf(string_buffer, "DHT: Temperature %hi.%01hi C, Humidity %i.%01i %% RH",
          temperature1/10, abs(temperature1%10),
          humidity/10, humidity%10);
      Serial.println(string_buffer);
      break;
    case DHT_ERROR_CHECKSUM:
      Serial.print("check sum error ");
      Serial.print(dht.getTemperatureC());
      Serial.print("C ");
      Serial.print(dht.getHumidity());
      Serial.println("%");
      break;
    case DHT_BUS_HUNG:
      Serial.println("BUS Hung ");
      break;
    case DHT_ERROR_NOT_PRESENT:
      Serial.println("Not Present ");
      break;
    case DHT_ERROR_ACK_TOO_LONG:
      Serial.println("ACK time out ");
      break;
    case DHT_ERROR_SYNC_TIMEOUT:
      Serial.println("Sync Timeout ");
      break;
    case DHT_ERROR_DATA_TIMEOUT:
      Serial.println("Data Timeout ");
      break;
    case DHT_ERROR_TOOQUICK:
      Serial.println("Polled to quick ");
      break;
    }
    dht11_starttime = millis();
  }
#endif

#ifdef USE_MQ5
  if (elapsedTime(mq5_starttime) > mq5_sampletime_ms) {
    int sensorValue = analogRead(MQ5_INPUT);
    gaz=(float)sensorValue/1024*5.0*10;

    if (gaz > 10) {
      buzzer_alert = 100;
    } else if (gaz > 25) {
      buzzer_alert = 250;
    } else {
      buzzer_alert = 0;
    }

    Serial.print("MQ5: Air quality = ");
    Serial.println(gaz,1);
    mq5_starttime = millis();
  }
#endif

#ifdef USE_MQ7
  if (elapsedTime(mq7_starttime) > mq7_timer_ms) {
    if (mq7_state == HIGH) {
      mq7_state = LOW;
      mq7_timer_ms = mq7_sampletime_ms;
      cogaz = 0; // handled as no data
    } else {
      mq7_state = HIGH;
      mq7_timer_ms = mq7_heattime_ms;

      // Note: first reading only available after 2.5 minutes
      int sensorValue = analogRead(MQ7_INPUT);
      cogaz = (float)sensorValue/1024*5.0*10;
    }
    digitalWrite(MQ7_TOG_DPIN, mq7_state);

    if (cogaz > 0) {
      Serial.print("MQ7: Air quality = ");
      Serial.println(cogaz,1);
    }
    mq7_starttime = millis();
  }
#endif

  if (elapsedTime(chibi_starttime) > chibi_sendtime_ms) {
    wdt_enable(WDTO_4S);
    char pressureS[16];
    char dust_ratioS[16];
    char dust_concentrationS[16];

    dtostrf(pressure/100, 0, 2, pressureS);
    dtostrf(dust_ratio, 0, 3, dust_ratioS);
    dtostrf(dust_concentration, 0, 3, dust_concentrationS);

    sprintf(string_buffer, "%hi.%01hi,%hi.%01hi,%s,%hi.%01hi,%hi.%01hi,%s,%s,%hi.%01hi",
        temperature1/10, temperature1%10,
        humidity/10, humidity%10,
        pressureS,
        temperature2/10, temperature2%10,
        gaz/10, gaz%10,
        dust_ratioS,
        dust_concentrationS,
        cogaz/10, cogaz%10);
    Serial.println("------------------------------------------------------");
    Serial.println(string_buffer);
    chibiTx(RECEIVER_ADDRESS, (uint8_t*)string_buffer, strlen(string_buffer)+1);
    Serial.println("------------------------------------------------------");
    chibi_starttime = millis();

    wdt_disable(); // disable the watchdog
    delay(1000);
  }

#ifdef USE_BUSZZER
  if (buzzer_alert) {
    digitalWrite(BUZZER_DPIN, HIGH);
    delay(buzzer_alert);
    digitalWrite(BUZZER_DPIN, LOW);
  }
#endif

  //Serial.print("freeMemory()=");
  //Serial.println(freeMemory());
  delay(50);
}


