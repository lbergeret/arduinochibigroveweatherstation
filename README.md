# Welcome to the Chibi Weather Station project
This project is powered by Freakduino Chibi 900Mhz boards

![chibi_station](https://bitbucket.org/lbergeret/arduinochibigroveweatherstation/raw/1bc1b3c9764b78823f12694ca4ce1ff4ca9e5a9a/assembly/overview.jpg)

# Sensors used
* Temperature/Humidity: DHT22
* Pressure: BMP085
* Gaz: MQ5
* Dust: PPD42NS

# Futur sensors
* MQ-7 Carbon Monoxide Sensor
* TP-401A Indoor Air Quality

# Links
* http://www.freaklabsstore.com
* http://www.seeedstudio.com/depot/grove-dust-sensor-p-1050.html?cPath=144_151
